﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Map {

    Tile[,] tiles;
    List<Character> characters;

    Dictionary<string, Furniture> furniturePrototypes;

    public int Width { get; protected set; }
    public int Height { get; protected set; }

    Action<Furniture> cbFurnitureCreated;
    Action<Furniture> cbFurnitureDestroyed;
    Action<Character> cbCharacterCreated;
    Action<Tile> cbTileChanged;

    // TODO: Most likely this will be replaced with a dedicated
    // class for managing job queues (plural!) that might also
    // be semi-static or self initializing or some damn thing.
    // For now, this is just a PUBLIC member of World
    public JobQueue jobQueue;

    public Map(int width = 100, int height = 100) {
        jobQueue = new JobQueue();

        Width = width;
        Height = height;

        tiles = new Tile[width, height];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                tiles[x, y] = new Tile(this, x, y);
                tiles[x, y].RegisterTileTypeChangedcb(OnTileChanged);
            }
        }

        Debug.Log ("Map created with " + (width * height) + " tiles.");

        characters = new List<Character>();

        CreateFurniturePrototypes();
    }

    public void Update(float deltaTime)
    {
        //Debug.Log("Update -- deltaTime: " + deltaTime);

        foreach (Character c in characters)
        {
            c.Update(deltaTime);
        }

    }

    public Character CreateCharacter(Tile t)
    {
        Character c = new Character(t);

        characters.Add(c);

        if (cbCharacterCreated != null)
            cbCharacterCreated(c);

        return c;
    }

    void CreateFurniturePrototypes()
    {
        furniturePrototypes = new Dictionary<string, Furniture>();

        furniturePrototypes.Add("c_wall",
            Furniture.CreatePrototype(
                                "c_wall",
                                0,  // Impassable
                                1,  // Width
                                1,  // Height
                                true // (Sprite) Links to neighbours and "sort of" becomes part of a large object
                            )
        );
        furniturePrototypes.Add("c_marblepath",
            Furniture.CreatePrototype(
                                "c_marblepath",
                                1,  // Passable
                                1,  // Width
                                1,  // Height
                                false // (Sprite) Does not link to neighbours and "sort of" becomes part of a large object
                            )
        );
    }

    //function for testing map gen
    public void RandomizeTiles() {
        for (int x = 0; x < Width; x++) {
            for (int y = 0; y < Height; y++) {
                if (UnityEngine.Random.Range(0,2) == 0) {
                    tiles[x, y].Type = TileType.Grass;
                }
                else {
                    tiles[x, y].Type = TileType.Dirt;
                }
            }
        }

        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                if (UnityEngine.Random.Range(0, 49) == 0)
                {
                    tiles[x, y].Type = TileType.Water;
                    tiles[x, y].RegisterTileTypeChangedcb(OnTileChanged);
                    if (x + 1 < Width) {tiles[x + 1, y].Type = TileType.Water; }
                    if (y + 1 < Height) { tiles[x, y + 1].Type = TileType.Water; }
                    if (x + 1 < Width && y + 1 < Height) {tiles[x + 1, y + 1].Type = TileType.Water; }
                }
            }
        }
    }

    public Tile GetTileAt(int x, int y) {
        if (x > Width || x < 0 || y > Height || y < 0) {
            //Debug.LogError("Tile (" + x + "," + y + ") is out of range.");
            return null;
        }
        return tiles[x, y];
    }

    public void PlaceFurniture(string objectType, Tile t)
    {
        Debug.Log("PlaceFurniture");
        // TODO: This function assumes 1x1 tiles -- change this later!

        if (furniturePrototypes.ContainsKey(objectType) == false)
        {
            Debug.LogError("furniturePrototypes doesn't contain a proto for key: " + objectType);
            return;
        }

        Furniture obj = Furniture.PlaceInstance(furniturePrototypes[objectType], t);

        if (obj == null)
        {
            // Failed to place object -- most likely there was already something there.
            return;
        }

        if (cbFurnitureCreated != null)
        {
            cbFurnitureCreated(obj);
        }
    }

    public void RemoveFurniture(Tile t)
    {
        // remove from map, remove furniture GameObject, remove entry from furnitureGameObjectMap
        if (cbFurnitureDestroyed != null)
        {
            cbFurnitureDestroyed(t.furniture);
        }
        t.RemoveFurniture();
    }

    public void RegisterFurnitureCreated(Action<Furniture> callbackfunc)
    {
        cbFurnitureCreated += callbackfunc;
    }

    public void RegisterFurnitureDestroyed(Action<Furniture> callbackfunc)
    {
        cbFurnitureDestroyed += callbackfunc;
    }

    public void RegisterCharacterCreated(Action<Character> callbackfunc)
    {
        cbCharacterCreated += callbackfunc;
    }

    public void UnregisterCharacterCreated(Action<Character> callbackfunc)
    {
        cbCharacterCreated -= callbackfunc;
    }

    public void UnregisterFurnitureCreated(Action<Furniture> callbackfunc)
    {
        cbFurnitureCreated -= callbackfunc;
    }

    public void RegisterTileChanged(Action<Tile> callbackfunc)
    {
        cbTileChanged += callbackfunc;
    }

    public void UnregisterTileChanged(Action<Tile> callbackfunc)
    {
        cbTileChanged -= callbackfunc;
    }

    void OnTileChanged(Tile t)
    {
        if (cbTileChanged == null)
            return;

        cbTileChanged(t);
    }

    public bool IsFurniturePlacementValid(string furnitureType, Tile t)
    {
        return furniturePrototypes[furnitureType].IsValidPosition(t);
    }
}
