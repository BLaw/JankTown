﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// TileType is the base type of the tile. In some tile-based games, that might be
// the terrain type. For us, we only need to differentiate between empty space
// and floor (a.k.a. the station structure/scaffold). Walls/Doors/etc... will be
// InstalledObjects sitting on top of the floor.
// public enum TileType { Dirt, Grass, Water, Empty };

public class Tile {

    private TileType _type = TileType.Empty;
    public TileType Type {
        get {return _type;}
        set {
            TileType oldType = _type;
            _type = value;
            //call the callback and let things know we've changed.

            if(cbTileTypeChanged !=null && oldType != _type)
                cbTileTypeChanged(this);
        }
    }

    // FIXME: This seems like a terrible way to flag if a job is pending
    // on a tile.  This is going to be prone to errors in set/clear.
    public Job pendingFurnitureJob;

    Inventory inventory;  //object that sits on the tile, e.g. loot
    public Furniture furniture { get; protected set; } //object like door, table, chair, etc. in quest or buildings and walls in city builder
    //MapObject mapObject; //background, un-interactable object? Trees, rocks, cliffs, doodads, etc.

    public Map map;

    public int X { get; protected set; }
    public int Y { get; protected set; }

	/// Initializes a new instance of the <see cref="Tile"/> class.
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
    public Tile( Map map, int x, int y ) {
        this.map = map;
        this.X = x;
        this.Y = y;
    }

    // The function we callback any time our type changes
    Action<Tile> cbTileTypeChanged;

    // Register a function to be called back when our tile type changes.
    public void RegisterTileTypeChangedcb(Action<Tile> callback) {
        cbTileTypeChanged += callback;
    }

    // Unregister a callback.
    public void UnRegisterTileTypeChangedcb(Action<Tile> callback) {
        cbTileTypeChanged -= callback;
    }

    public bool PlaceFurniture(Furniture objInstance)
    {
        if (objInstance == null)
        {
            // We are uninstalling whatever was here before.
            furniture = null;
            return true;
        }

        // objInstance isn't null

        if (furniture != null)
        {
            Debug.LogError("Trying to assign a furniture to a tile that already has one!");
            return false;
        }

        // At this point, everything's fine!

        furniture = objInstance;
        return true;
    }

    public void RemoveFurniture()
    {
        furniture = null;
    }

    // Tells us if two tiles are adjacent.
    public bool IsNeighbour(Tile tile, bool diagOkay = false)
    {
        if (this.X == tile.X && (this.Y == tile.Y + 1 || this.Y == tile.Y - 1))
            return true;

        if (this.Y == tile.Y && (this.X == tile.X + 1 || this.X == tile.X - 1))
            return true;

        if (diagOkay)
        {
            if (this.X == tile.X + 1 && (this.Y == tile.Y + 1 || this.Y == tile.Y - 1))
                return true;
            if (this.X == tile.X - 1 && (this.Y == tile.Y + 1 || this.Y == tile.Y - 1))
                return true;
        }

        return false;
    }
}
