﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Idea of this is to take coordinates from the tiles as 
 * data (cartesian) to tiles as graphics (GameObjects, isometric tiles)
 * Cartesian starts at bottom left. +X to the right, +Y to the top
 * Isometric should start at left center, +X diagonally down, +Y diagonally up.
 * Isometric grid needs to be drawn from the top down to properly render things,
 * although maybe unity will take care of it with the sorting layer property of the GameObject SpriteRender?
 *                          __
 *      /\                   /|
 *       |               +Y / 
 *     +Y|____>            /____>
 *         +X                +X
 *    Cartesian         Isometric
 */

public class IsoConvert {
    /*
        public void TileDataToIsometric(Vector3 tileMapPosition) {

        public float tileWidth = 1;
        public float tileHeight = 0.5f;

        public float tileIsoX = (((tileMapPosition.X * tileWidth) / 2) + ((tileMapPosition.Y * tileWidth) / 2));
        public float tileIsoY = (((tileMapPosition.Y * tileHeight) / 2) - ((tileMapPosition.X * tileHeight) / 2));
        }

        public void Rotate45DoubleY(Vector3 coord) {
        int x = Mathf.FloorToInt((((1 * coord.x) * Mathf.Cos(Mathf.PI / 4)) - ((2 * coord.y) * Mathf.Sin(Mathf.PI / 4))) * Mathf.Sqrt(2));
        int y = Mathf.FloorToInt((((1 * coord.x) * Mathf.Sin(Mathf.PI / 4)) + ((2 * coord.y) * Mathf.Cos(Mathf.PI / 4))) * Mathf.Sqrt(2));
        }
    */
}
