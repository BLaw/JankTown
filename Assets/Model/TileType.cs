﻿using System;
using System.Collections.Generic;


public class TileType : IComparable
{
    public static TileType Dirt = new TileType("Dirt", 1.0f);
    public static TileType Grass = new TileType("Grass", 1.0f);
    public static TileType Water = new TileType("Water", 0.25f, true);
    public static TileType Mud = new TileType("Mud", 0.25f, true);
    public static TileType Empty = new TileType("Empty", 0.0f);


    private static int _lastId = -1;
    private readonly int _id;

    public readonly string Name;
    public readonly float MoveMultiplier;
    public readonly bool LinksToNeighbor;

    private static volatile Dictionary<string, TileType> tileTypeNameDict;

    private static Dictionary<string, TileType> GetTypeDict() 
    {
        if (tileTypeNameDict == null)
        {
            tileTypeNameDict = new Dictionary<string, TileType>();
        }
        return tileTypeNameDict;
    }

    public TileType()
    {
    }

    protected TileType(string name, 
                       float mult, 
                       bool links_to_neighbor=false)
    {
        _id = ++_lastId;
        Name = name;
        MoveMultiplier = mult;
        LinksToNeighbor = links_to_neighbor;

        GetTypeDict().Add(name, this);
    }

    public int CompareTo(object obj)
    {
        return _id.CompareTo(obj);
    }

    public static implicit operator int(TileType x)
    {
        return x._id;
    }

    public static implicit operator TileType(string s)
    {
        return GetTypeDict()[s] ?? TileType.Empty;
    }

    public override string ToString()
    {
        return Name;
    }

}
