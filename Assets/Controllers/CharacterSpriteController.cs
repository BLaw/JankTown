﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterSpriteController : MonoBehaviour {

	Dictionary<Character, GameObject> characterGameObjectMap;

	Dictionary<string, Sprite> characterSprites;

	Map map {
		get { return MapController.Instance.map; }
	}

	// Use this for initialization
	void Start () {
		LoadSprites();

		// Instantiate our dictionary that tracks which GameObject is rendering which character.
		characterGameObjectMap = new Dictionary<Character, GameObject>();

		// Register our callback so that our GameObject gets updated whenever
		// the tile's type changes.
		map.RegisterCharacterCreated(OnCharacterCreated);



		// DEBUG
		Character c = map.CreateCharacter(map.GetTileAt(map.Width/2, map.Height/2 ) );

		//c.SetDestination( world.GetTileAt( world.Width/2 + 5, world.Height/2 ) );
	}

	void LoadSprites() {
		characterSprites = new Dictionary<string, Sprite>();
		Sprite[] sprites = Resources.LoadAll<Sprite>("Images/Characters/");

		Debug.Log("LOADED RESOURCE:");
		foreach(Sprite s in sprites) {
			Debug.Log(s);
			characterSprites[s.name] = s;
		}
	}

	public void OnCharacterCreated( Character c ) {
		Debug.Log("OnCharacterCreated");
		// Create a visual GameObject linked to this data.

		// FIXME: Does not consider multi-tile objects nor rotated objects

		// This creates a new GameObject and adds it to our scene.
		GameObject char_go = new GameObject();

		// Add our tile/GO pair to the dictionary.
		characterGameObjectMap.Add( c, char_go );

		char_go.name = "Character";

        float tileIsoX, tileIsoY;
        MapController.cart2iso(c.currTile, out tileIsoX, out tileIsoY);
        char_go.transform.position = new Vector3(tileIsoX + 0.5f, tileIsoY - 0.25f, 0);
		char_go.transform.SetParent(this.transform, true);

		SpriteRenderer sr = char_go.AddComponent<SpriteRenderer>();
		sr.sprite = characterSprites["temp-character"];
		sr.sortingLayerName = "characters";

		// Register our callback so that our GameObject gets updated whenever
		// the object's into changes.
		c.RegisterOnChangedCallback( OnCharacterChanged );

	}

	void OnCharacterChanged( Character c ) {

		if(characterGameObjectMap.ContainsKey(c) == false) {
			Debug.LogError("OnCharacterChanged -- trying to change visuals for character not in our map.");
			return;
		}

		GameObject char_go = characterGameObjectMap[c];

        float tileWidthHalf = 0.5f;
        float tileHeightHalf = 0.25f;

        float tileIsoX = ((c.X + c.Y) * tileWidthHalf);
        float tileIsoY = ((c.Y - c.X) * tileHeightHalf);
        char_go.transform.position = new Vector3(tileIsoX + 0.5f, tileIsoY - 0.25f, 0);
	}


	
}
