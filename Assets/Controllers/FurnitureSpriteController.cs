﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

public class FurnitureSpriteController : MonoBehaviour {

	Dictionary<Furniture, GameObject> furnitureGameObjectMap;

	Dictionary<string, Sprite> furnitureSprites;

    public string furnLayer;

    Map map {
		get { return MapController.Instance.map; }
	}

	// Use this for initialization
	void Start () {
		LoadSprites();

		// Instantiate our dictionary that tracks which GameObject is rendering which Tile data.
		furnitureGameObjectMap = new Dictionary<Furniture, GameObject>();

		// Register our callback so that our GameObject gets updated whenever
		// the tile's type changes.
		map.RegisterFurnitureCreated(OnFurnitureCreated);
        map.RegisterFurnitureDestroyed(OnFurnitureDestroyed);
    }

	void LoadSprites() {
		furnitureSprites = new Dictionary<string, Sprite>();
		Sprite[] sprites = Resources.LoadAll<Sprite>("Images/Furniture/");

		//Debug.Log("LOADED RESOURCE:");
		foreach(Sprite s in sprites) {
            furnitureSprites[s.name] = s;
            //Debug.Log(s);
		}
	}

	public void OnFurnitureCreated( Furniture furn ) {
        Debug.Log("OnFurnitureCreated" + " " + "(" + furn.tile.X + "," + furn.tile.Y + ")");
        // Create a visual GameObject linked to this data.

        // FIXME: Does not consider multi-tile objects nor rotated objects

        // This creates a new GameObject and adds it to our scene.
        GameObject furn_go = new GameObject();

		// Add our tile/GO pair to the dictionary.
		furnitureGameObjectMap.Add( furn, furn_go );

		furn_go.name = furn.objectType + "_" + furn.tile.X + "_" + furn.tile.Y;
        float tileIsoX, tileIsoY;
        MapController.cart2iso(furn.tile, out tileIsoX, out tileIsoY);
        furn_go.transform.position = new Vector3(tileIsoX + 0.5f, tileIsoY - 0.25f, 0);

        GameObject furnTile = TileSpriteController.tileGameObjectMap[furn.tile];
        furn_go.transform.SetParent(furnTile.transform, true);

        furn_go.AddComponent<SpriteRenderer>().sprite = GetSpriteForFurniture(furn);
        furn_go.GetComponent<SpriteRenderer>().sortingLayerName = furnLayer;
        furn_go.GetComponent<SpriteRenderer>().sortingOrder = furnTile.GetComponent<SpriteRenderer>().sortingOrder;

        // Register our callback so that our GameObject gets updated whenever
        // the object's into changes.
        furn.RegisterOnChangedCallback( OnFurnitureChanged );

	}

    public void OnFurnitureDestroyed( Furniture furn )
    {
        Debug.Log("OnFurnitureDestroyed" + " " + "(" + furn.tile.X + "," + furn.tile.Y + ")");
        GameObject furn_go = furnitureGameObjectMap[furn];
        furn_go.GetComponent<SpriteRenderer>().sprite = null;
        furnitureGameObjectMap.Remove(furn);
        furn.UnregisterOnChangedCallback(OnFurnitureChanged);
        Destroy(furn_go);
    }

	void OnFurnitureChanged( Furniture furn ) {
		//Debug.Log("OnFurnitureChanged");
		// Make sure the furniture's graphics are correct.

		if(furnitureGameObjectMap.ContainsKey(furn) == false) {
			Debug.LogError("OnFurnitureChanged -- trying to change visuals for furniture not in our map.");
			return;
		}

		GameObject furn_go = furnitureGameObjectMap[furn];
		//Debug.Log(furn_go);
		//Debug.Log(furn_go.GetComponent<SpriteRenderer>());

		furn_go.GetComponent<SpriteRenderer>().sprite = GetSpriteForFurniture(furn);
	}

	public Sprite GetSpriteForFurniture(Furniture obj) {
		if(obj.linksToNeighbour == false) {
			return furnitureSprites[obj.objectType];
		}

		// Otherwise, the sprite name is more complicated.

		string spriteName = obj.objectType + "_";

		// Check for neighbours North, East, South, West

		int x = obj.tile.X;
		int y = obj.tile.Y;

		Tile t;

		t = map.GetTileAt(x, y + 1);
		if(t != null && t.furniture != null && t.furniture.objectType == obj.objectType) {
			spriteName += "N";
		}
		t = map.GetTileAt(x + 1, y);
		if(t != null && t.furniture != null && t.furniture.objectType == obj.objectType) {
			spriteName += "E";
		}
		t = map.GetTileAt(x, y - 1);
		if(t != null && t.furniture != null && t.furniture.objectType == obj.objectType) {
			spriteName += "S";
		}
		t = map.GetTileAt(x - 1, y);
		if(t != null && t.furniture != null && t.furniture.objectType == obj.objectType) {
			spriteName += "W";
		}

		// For example, if this object has all four neighbours of
		// the same type, then the string will look like:
		//       Wall_NESW

		if(furnitureSprites.ContainsKey(spriteName) == false) {
			Debug.LogError("GetSpriteForInstalledObject -- No sprites with name: " + spriteName);
			return null;
		}

		return furnitureSprites[spriteName];

	}

    public Sprite GetSpriteForFurniture(string objectType)
    {
        if (furnitureSprites.ContainsKey(objectType))
        {
            return furnitureSprites[objectType];
        }

        if (furnitureSprites.ContainsKey(objectType + "_"))
        {
            return furnitureSprites[objectType + "_"];
        }

        Debug.LogError("GetSpriteForFurniture -- No sprites with name: " + objectType);
        return null;
    }

}
