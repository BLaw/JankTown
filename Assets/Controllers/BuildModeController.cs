﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildModeController : MonoBehaviour {

    enum BuildMode { Objects, Demolish, SetTiles };
    BuildMode buildMode;
    TileType buildModeTile = TileType.Grass;
    string buildModeObjectType;

    // Use this for initialization
    void Start () {
		
	}

    public void SetMode_BuildFurniture(string objectType)
    {
        Debug.Log("Objects Mode (furniture)");
        // Wall is not a Tile!  Wall is a "Furniture" that exists on TOP of a tile.
        buildMode = BuildMode.Objects;
        buildModeObjectType = objectType;
    }

    public void SetMode_Demolish()
    {
        Debug.Log("Demolish Mode");
        buildMode = BuildMode.Demolish;
        //buildModeTile = TileType.Dirt;
    }

    public void SetMode_Tile(string bmTileType)
    {
        Debug.Log("Tile Mode");
        buildMode = BuildMode.SetTiles;

        buildModeTile = bmTileType;
    }

    public void DoBuild( Tile t)
    {
        switch (buildMode)
        {
            case BuildMode.Objects:
                Debug.Log("Mode: " + buildMode);
                // (old) Create the Furniture and assign it to the tile
                // MapController.Instance.map.PlaceFurniture(buildModeObjectType, t);

                // (new) Create a job for tile, add it to job queue
                string furnitureType = buildModeObjectType;

                if (
                MapController.Instance.map.IsFurniturePlacementValid(furnitureType, t) &&
                t.pendingFurnitureJob == null
                    )
                {
                    // This tile position is valid for this furniture
                    // Create a job for it to be build

                    Job j = new Job(t, furnitureType, (theJob) => {
                        MapController.Instance.map.PlaceFurniture(furnitureType, theJob.tile);

                        // FIXME: I don't like having to manually and explicitly set
                        // flags that preven conflicts. It's too easy to forget to set/clear them!
                        t.pendingFurnitureJob = null;
                    }
                    );

                    // FIXME: I don't like having to manually and explicitly set
                    // flags that preven conflicts. It's too easy to forget to set/clear them!
                    t.pendingFurnitureJob = j;
                    j.RegisterJobCancelCallback((theJob) => { theJob.tile.pendingFurnitureJob = null; });

                    // Add the job to the queue
                    MapController.Instance.map.jobQueue.Enqueue(j);

                    //Debug.Log("Job Queue Size: " + MapController.Instance.map.jobQueue.Count);
                }

                break;
            case BuildMode.Demolish:
                if (t.furniture != null)
                {
                    MapController.Instance.map.RemoveFurniture(t);
                }
                break;
            case BuildMode.SetTiles:
                // change Tile t TileType
                t.Type = buildModeTile;
                break;
            default:
                // ????
                Debug.Log("Mode: " + buildMode);
                break;
        }
    }

}
