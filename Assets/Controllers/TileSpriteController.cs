﻿//using System.Collections;
//using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using System.Text.RegularExpressions;


public class TileSpriteController : MonoBehaviour {

    //need a better way to handle this (tile types)
    //include with LoadSprites

    public Sprite dirtSprite;
    public Sprite dirtSprite2;
    public Sprite grassSprite;
    public Sprite grassSprite2;
    public Sprite waterSprite;
    public Sprite gridSprite;
    public Sprite emptySprite;

    Dictionary<string, Sprite> tileSprites;
    public static Dictionary<Tile, GameObject> tileGameObjectMap;
    public static Dictionary<Tile, GameObject> tileGameObjectOverlayMap;

    //For sprite ordering
    public string tileLayer;

    Map map {
        get { return MapController.Instance.map; }
    }

    // Use this for initialization
    void Start ()
    {

        LoadSprites();

        //tracks which GameObject is rendering which tile
        tileGameObjectMap = new Dictionary<Tile, GameObject>();
        //tracks which overlay GameObject is rendering which tile
        tileGameObjectOverlayMap = new Dictionary<Tile, GameObject>();

        int tileSortingOrder;
        tileSortingOrder = 0;

        //Create GameObject for each tile
        for (int x = 0; x < map.Width; x++)
        {
            for (int y = map.Height - 1; y >= 0; y--)
            {
                Tile tile_data = map.GetTileAt(x, y);

                //create tile GameObject, map it to tile data, name it, place it, give it a sprite w/ sorting #
                GameObject tile_go = new GameObject();
                tileGameObjectMap.Add(tile_data, tile_go);
                tile_go.name = "Tile_" + x + "_" + y;
                float tileIsoX, tileIsoY;
                MapController.cart2iso(tile_data, out tileIsoX, out tileIsoY);
                tile_go.transform.position = new Vector3(tileIsoX + 0.5f, tileIsoY - 0.25f, 0); //offset due to sprite base point at (0.5,0) instead of (0,0.25)
                tile_go.transform.SetParent(this.transform, true);

                //add sprite renderer, set default sprite
                tile_go.AddComponent<SpriteRenderer>().sprite = gridSprite;

                //set sortinglayer and order in layer
                tile_go.GetComponent<SpriteRenderer>().sortingLayerName = tileLayer;

                tileSortingOrder = tileSortingOrder + 1;
                tile_go.GetComponent<SpriteRenderer>().sortingOrder = tileSortingOrder;

                //add child gameobject to hold sprite renderer for tile type overlays, e.g. water
                GameObject tile_go_overlay = new GameObject();
                tileGameObjectOverlayMap.Add(tile_data, tile_go_overlay);

                tile_go_overlay.name = "Tile_" + x + "_" + y + "overlay";
                tile_go_overlay.transform.position = new Vector3(tileIsoX + 0.5f, tileIsoY - 0.25f, 0); //offset due to sprite base point at (0.5,0) instead of (0,0.25)
                tile_go_overlay.transform.SetParent(tile_go.transform, true);
                tile_go_overlay.AddComponent<SpriteRenderer>().sprite = emptySprite;
                tile_go_overlay.GetComponent<SpriteRenderer>().sortingLayerName = tileLayer;
                tile_go_overlay.GetComponent<SpriteRenderer>().sortingOrder = tileSortingOrder + 1;

                tile_data.RegisterTileTypeChangedcb(OnTileChanged);
            }
        }
        // for testing, replace with thing that loads map config or something
        map.RandomizeTiles();
        for (int x = 0; x < map.Width; x++)
        {
            for (int y = map.Height - 1; y >= 0; y--)
            {
                Tile tile_data = map.GetTileAt(x, y);
                OnTileChanged(tile_data);
            }
        }

        map.RegisterTileChanged(OnTileChanged);
    }

    void LoadSprites()
    {
        tileSprites = new Dictionary<string, Sprite>();
        Sprite[] sprites = Resources.LoadAll<Sprite>("Images/Tiles/");

        //Debug.Log("LOADED RESOURCE:");
        foreach (Sprite s in sprites)
        {
            //Debug.Log(s);
            tileSprites[s.name] = s;
        }
    }

    // This function should be called automatically whenever a tile's type gets changed.
    void OnTileChanged(Tile tile_data)
    {
        if(tileGameObjectMap.ContainsKey(tile_data) == false)
        {
            Debug.LogError("tileGameObjectMap doesn't contain the tile_data. Did you forget to add to dictionary, or unregister callback?");
            return;
        }
        GameObject tile_go = tileGameObjectMap[tile_data];
        if (tile_go == null)
        {
            Debug.LogError("tileGameObjectMap returned GameObject is null. Did you forget to add to dictionary, or unregister callback?");
            return;
        }
        GameObject tile_go_overlay = tileGameObjectOverlayMap[tile_data];
        if (tile_go_overlay == null)
        {
            Debug.LogError("tileGameObjectOverlayMap returned GameObject is null. Did you forget to add to dictionary, or unregister callback?");
            return;
        }

        // Check if tile has furniture, if so check validity of furniture on tile, destroy furniture if not valid.
        if (tile_data.furniture != null)
        {
            if ( tile_data.furniture.IsValidPosition(tile_data) == false)
            {
                MapController.Instance.map.RemoveFurniture(tile_data);
            }
        }

        //assign sprite per tile type.
        if (tile_data.Type == TileType.Dirt)
        {
            tile_go_overlay.GetComponent<SpriteRenderer>().sprite = emptySprite;

            if (Random.Range(0, 2) == 0)
            {
                tile_go.GetComponent<SpriteRenderer>().sprite = dirtSprite;
            }
            else
            {
                tile_go.GetComponent<SpriteRenderer>().sprite = dirtSprite2;
            }
        }
        else if (tile_data.Type == TileType.Grass)
        {
            tile_go_overlay.GetComponent<SpriteRenderer>().sprite = emptySprite;

            if (Random.Range(0, 2) == 0)
            {
                tile_go.GetComponent<SpriteRenderer>().sprite = grassSprite;
            }
            else
            {
                tile_go.GetComponent<SpriteRenderer>().sprite = grassSprite2;
            }
        }
        else if (tile_data.Type == TileType.Water)
        {
            tile_go_overlay.GetComponent<SpriteRenderer>().sprite = waterSprite;
            //tile_go_overlay.GetComponent<SpriteRenderer>().sprite = GetSpriteForTile(tile_data);
        }
        else if (tile_data.Type == TileType.Empty)
        {
            tile_go_overlay.GetComponent<SpriteRenderer>().sprite = emptySprite;

            tile_go.GetComponent<SpriteRenderer>().sprite = gridSprite;
        }
        else
        {
            Debug.LogError("OnTileTypeChanged - Unrecognized tile type.");
        }
    }

    //Example function, not currently used. Maybe use it when changing floors/level.
    //Destroys all visual GameObjects, but not the tile data
    public void DestroyAllTileGameObjects()
    {
        while (tileGameObjectMap.Count > 0)
        {
            Tile tile_data = tileGameObjectMap.Keys.First();
            GameObject tile_go = tileGameObjectMap[tile_data];

            //remove pair from map
            tileGameObjectMap.Remove(tile_data);

            //unregister the callback
            tile_data.UnRegisterTileTypeChangedcb(OnTileChanged);

            //destroy visual GameObject
            Destroy(tile_go);
        }
        //call something to make/load the new tiles.
    }
}
