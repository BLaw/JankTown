﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobSpriteController : MonoBehaviour {

    // Right now, mostly piggybacks on FuritureSpriteController
    // Sprite for job will be furniture sprite turned mono-chrome yellow? Green?
    // also semi-transparent

    FurnitureSpriteController fsc;
    Dictionary<Job, GameObject> jobGameObjectMap;

    // Use this for initialization
    void Start () {
        jobGameObjectMap = new Dictionary<Job, GameObject>();
        fsc = GameObject.FindObjectOfType<FurnitureSpriteController>();

        // queue job
        MapController.Instance.map.jobQueue.RegisterJobCreationCallback(OnJobCreated);
	}
	
    void OnJobCreated(Job job)
    {
        // FIXME: We can only do furniture-building jobs.

        // TODO: Sprite

        GameObject job_go = new GameObject();

        // Add our tile/GO pair to the dictionary.
        jobGameObjectMap.Add(job, job_go);

        job_go.name = "JOB_" + job.jobObjectType + "_" + job.tile.X + "_" + job.tile.Y;
        float tileIsoX, tileIsoY;
        MapController.cart2iso(job.tile, out tileIsoX, out tileIsoY);
        job_go.transform.position = new Vector3(tileIsoX + 0.5f, tileIsoY - 0.25f, 0);

        GameObject jobTile = TileSpriteController.tileGameObjectMap[job.tile];
        job_go.transform.SetParent(jobTile.transform, true);

        SpriteRenderer sr = job_go.AddComponent<SpriteRenderer>();
        sr.sprite = fsc.GetSpriteForFurniture(job.jobObjectType);
        sr.color = new Color(0.5f, 1f, 0.5f, 0.5f);

        job_go.GetComponent<SpriteRenderer>().sortingLayerName = "job";
        job_go.GetComponent<SpriteRenderer>().sortingOrder = jobTile.GetComponent<SpriteRenderer>().sortingOrder;

        job.RegisterJobCompleteCallback(OnJobEnded);
        job.RegisterJobCancelCallback(OnJobEnded);
    }
	
    void OnJobEnded(Job job)
    {
        // This executes whether a job was COMPLETED or CANCELLED

        // FIXME: We can only do furniture-building jobs.

        GameObject job_go = jobGameObjectMap[job];

        job.UnregisterJobCompleteCallback(OnJobEnded);
        job.UnregisterJobCancelCallback(OnJobEnded);

        Destroy(job_go);
    }

}
