﻿//using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour {

    public static MapController Instance { get; protected set; }

    //Map and Tile data
    public Map map { get; protected set; }

    // Use this for initialization
    void OnEnable () {

        if(Instance != null) {
            Debug.LogError("World already exists??");
        }
        Instance = this;

        map = new Map();

        // Center the Camera
        float mapIsoWidth = map.Width;
        float mapIsoHeight = map.Height * 0;
        Camera.main.transform.position = new Vector3(mapIsoWidth / 2, mapIsoHeight / 2, Camera.main.transform.position.z);

        // for testing, replace with thing that loads map config or something
        //map.RandomizeTiles();
	}

    void Update()
    {
        // TODO?: add pause/unpause, speed controls, etc?
        map.Update(Time.deltaTime);
    }

    /// Gets the tile at the unity-space coordinates
	/// <returns>The tile at world coordinate.</returns>
	/// <param name="coord">Unity World-Space coordinates.</param>
    /// 

    public Tile GetTileAtMapCoord(Vector3 coord) {

        // Rotate 45 degrees, double Y

        int x = Mathf.FloorToInt(((coord.x / Mathf.Sqrt(2)) - ((2 * coord.y) / Mathf.Sqrt(2))) * Mathf.Sqrt(2));
        int y = Mathf.FloorToInt(((coord.x / Mathf.Sqrt(2)) + ((2 * coord.y) / Mathf.Sqrt(2))) * Mathf.Sqrt(2));

        return map.GetTileAt(x, y);

        // simplified version of the above,
        // see http://clintbellanger.net/articles/isometric_math/

        // I don't know why this doesn't work...

        //float tileWidthHalf = 0.5f;
        //float tileHeightHalf = 0.25f;
        //int x = Mathf.FloorToInt(((coord.x / tileWidthHalf) + (coord.y / tileHeightHalf)) / 2);
        //int y = Mathf.FloorToInt(((coord.x / tileWidthHalf) - (coord.y / tileHeightHalf)) / 2);

        //Debug.Log("GetTileAt(" + x + "," + y + ")");
        //return Map.GetTileAt(x, y);
    }

    public static void cart2iso(Tile tileUnderMouse, out float tileIsoX, out float tileIsoY)
    {
        // cartesian->isometric conversion, actual tile pixel dimensions are set in unity on the sprite,
        // tile width and height here is 64x32, actual full sprite is 64x64px
        // used to convert from tile array generated in map.cs to isometric gameObjects in Unity
        // see http://clintbellanger.net/articles/isometric_math/ for how it was derived
        float tileWidthHalf = 0.5f;
        float tileHeightHalf = 0.25f;

        tileIsoX = ((tileUnderMouse.X + tileUnderMouse.Y) * tileWidthHalf);
        tileIsoY = ((tileUnderMouse.Y - tileUnderMouse.X) * tileHeightHalf);
    }

}
